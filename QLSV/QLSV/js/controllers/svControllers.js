function layThongTin(){
    let _maSV = document.querySelector("#txtMaSV").value;
    let _tenSV = document.querySelector("#txtTenSV").value;
    let _email = document.querySelector("#txtEmail").value;
    let _matKhau = document.querySelector("#txtPass").value;
    let _diemToan = document.querySelector("#txtDiemToan").value*1;
    let _diemLy = document.querySelector("#txtDiemLy").value*1;
    let _diemHoa = document.querySelector("#txtDiemHoa").value*1;

    let sv = new SinhVien(_maSV, _tenSV, _email, _matKhau, _diemToan, _diemLy, _diemHoa)
    console.log(sv)
    return sv;
}

function renderDSSV(svArr){
    let content ="";
    for(let index = 0; index<svArr.length; index++) {
        let DS = svArr[index]
        content = content + `
        <tr>
            <td>${DS.maSV}</td>
            <td>${DS.tenSV}</td>
            <td>${DS.email}</td>
            <td>${DS.tinhDTB()}</td>
            <button onclick="xoaSV('${DS.maSV}')" class="btn-primary mr-3">Xóa</button>
            <button onclick="suaNhanVien('${DS.maSV}')" class="btn-warning">Sữa</button>
        </tr>
        `
    }
    document.querySelector("#tbodySinhVien").innerHTML = content
}


function timViTri(id, arr){
    let viTri = -1;
    for (let index = 0; index < arr.length; index++) {
        let svArr = arr[index];
        if(svArr.maSV == id){
            viTri = index
        }
    }
    return viTri
}