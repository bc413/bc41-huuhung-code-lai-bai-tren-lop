let DSSV = [];

let getDSSV = localStorage.getItem("DSSV");
if(getDSSV != null){
    let dataArr = JSON.parse(getDSSV)
    DSSV = dataArr.map(function(item){
        var sv = new SinhVien(item.maSV,item.tenSV, item.email, item.matKhau, item.diemToan, item.diemLy,item.diemHoa)
        return sv;
    })
    renderDSSV(DSSV)
}


function themSinhVien(){
    DSSV.push(layThongTin())
    let dssvJson = JSON.stringify(DSSV)
    localStorage.setItem('DSSV', dssvJson);
    renderDSSV(DSSV)
};


function xoaSV(idSV){
    let viTri = timViTri(idSV, DSSV)
    if(viTri != -1){
        DSSV.splice(viTri,1)
        renderDSSV(DSSV)
    }
}

function suaNhanVien(idSV){
    let viTri = timViTri(idSV, DSSV)
    if(viTri != -1){
        let sv = DSSV[viTri]
        document.getElementById("txtMaSV").disabled = true;
        document.querySelector("#txtMaSV").value = `${sv.maSV}`
        document.querySelector("#txtTenSV").value = `${sv.tenSV}`
        document.querySelector("#txtEmail").value = `${sv.email}`
        document.querySelector("#txtPass").value = `${sv.matKhau}`
        document.querySelector("#txtDiemToan").value = `${sv.diemToan}`
        document.querySelector("#txtDiemLy").value = `${sv.diemLy}`
        document.querySelector("#txtDiemHoa").value = `${sv.diemHoa}`
    }
}

function capNhat(){
    document.getElementById("txtMaSV").disabled = false;
    let maSV = document.querySelector("#txtMaSV").value
    let viTri = timViTri(maSV, DSSV);
    
    if(viTri !== -1){
        DSSV[viTri].tenSV =  document.querySelector("#txtTenSV").value
        DSSV[viTri].email =  document.querySelector("#txtEmail").value
        DSSV[viTri].matKhau =  document.querySelector("#txtPass").value
        DSSV[viTri].diemToan =  document.querySelector("#txtDiemToan").value
        DSSV[viTri].diemLy =  document.querySelector("#txtDiemLy").value
        DSSV[viTri].diemHoa =  document.querySelector("#txtDiemHoa").value
        renderDSSV(DSSV)
    }
    let dssvJson = JSON.stringify(DSSV)
    localStorage.setItem('DSSV', dssvJson);
}
