function SinhVien(_maSV,_tenSV,_email,_matKhau,_diemToan,_diemLy,_diemHoa){
    this.maSV = _maSV;
    this.tenSV = _tenSV;
    this.email = _email;
    this.matKhau = _matKhau;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function(){
        return (this.diemToan*1 + this.diemLy*1 +  this.diemHoa*1)/3;
    }
}